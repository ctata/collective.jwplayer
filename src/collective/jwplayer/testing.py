# -*- coding: utf-8 -*-
from plone.app.contenttypes.testing import PLONE_APP_CONTENTTYPES_FIXTURE
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import (
    applyProfile,
    FunctionalTesting,
    IntegrationTesting,
    PloneSandboxLayer,
)
from plone.testing import z2

import collective.jwplayer


class CollectiveJwplayerLayer(PloneSandboxLayer):

    defaultBases = (PLONE_APP_CONTENTTYPES_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.restapi
        self.loadZCML(package=plone.restapi)
        self.loadZCML(package=collective.jwplayer)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'collective.jwplayer:default')


COLLECTIVE_JWPLAYER_FIXTURE = CollectiveJwplayerLayer()


COLLECTIVE_JWPLAYER_INTEGRATION_TESTING = IntegrationTesting(
    bases=(COLLECTIVE_JWPLAYER_FIXTURE,),
    name='CollectiveJwplayerLayer:IntegrationTesting',
)


COLLECTIVE_JWPLAYER_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(COLLECTIVE_JWPLAYER_FIXTURE,),
    name='CollectiveJwplayerLayer:FunctionalTesting',
)


COLLECTIVE_JWPLAYER_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        COLLECTIVE_JWPLAYER_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name='CollectiveJwplayerLayer:AcceptanceTesting',
)
