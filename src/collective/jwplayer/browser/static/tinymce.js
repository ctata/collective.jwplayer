(function ($) {

    //tinymce.PluginManager.add('jwplayer', tinymce.plugins.JWplayerPlugin);
    tinymce.PluginManager.add('jwplayer', function (editor, url) {

        function openDialog () {
            tinymce.activeEditor.jwplayerdata = null;


            var $node = $(editor.selection.getNode());
            parameters = {}
            if ($node.hasClass('jwp-tinymce-plone')) {
                tinymce.activeEditor.jwplayerdata = $node.data();
                parameters = $node.data();
            }

            return editor.windowManager.open({
                title: 'JWPlayer Video Player Settings',
                body: getDialogBody(parameters),
                //url: url + '/@@jwp-tinymce-settings',
                
                width: 890,
                height: 480,
                //inline: true,
                close_previous: "yes",
                onsubmit: function(eventApi) {
                    data = eventApi.data;
                    if (data.player_host_type == 'self-hosted' && (!data.player_self_url || !data.player_self_key)) {
                        editor.windowManager.alert('Please specify the self-hosted URL and Key')
                        return;
                    }
                    if (data.player_host_type == 'cloud-hosted' && !data.player_url) {
                        editor.windowManager.alert('Please specify the cloud-hosted URL')
                        return;
                    }
                    if (!data.video_url) {
                        editor.windowManager.alert('Please specify the video URL')
                        return;
                    }

                    data['meta'] = parseOptions(data.video_opts);
                    delete data['options']
                    insertPlaceholder(data)
                },
                onchange: function(eventApi) {
                    // do something when a field changes
                }
            }, parameters);
        };

        function getDialogBody(parameters) {
            var options = '';
            for (var m in parameters) {
                if (m.indexOf('meta') === 0) {
                    options += m.replace('meta_', '') + ' = ' + parameters[m] + "\n";
                }
            }

            return [
                {
                    type   : 'listbox',
                    name   : 'player_host_type',
                    label  : 'Playe Host Type',
                    values : [
                        { text: 'Use cloud player', value: 'could-hosted' },
                        { text: 'Use self-hosted player', value: 'self-hosted' }
                    ],
                    value: parameters.player_host_type || 'cloud-hosted'
                },
                {
                    type   : 'textbox',
                    name   : 'player_url',
                    label  : 'JW Player Cloud-Hosted URL',
                    value  : parameters.player_url || ''
                },
                {
                    type   : 'textbox',
                    name   : 'player_self_url',
                    label  : 'JW Player Self-Hosted URL',
                    value  : parameters.player_self_url || ''
                },
                {
                    type   : 'textbox',
                    name   : 'player_self_key',
                    label  : 'JW Player Self-Hosted Key',
                    tooltip : 'This is needed only if you are using a self-hosted player',
                    value  : parameters.player_self_key || ''
                },
                {
                    type   : 'textbox',
                    name   : 'video_url',
                    label  : 'Enter URL to video',
                    tooltip : 'Please provide the full URL to the video including the protocol',
                    value  : parameters.video_url || ''
                },
                {
                    type   : 'container',
                    name   : 'container',
                    label  : '',
                    tooltip: 'Click the link to see all possible options',
                    html   : '<a href="https://developer.jwplayer.com/jw-player/docs/developer-guide/customization/configuration-reference/#setup" target="_blank" i18n:translate="">Player Options (one per line)</a> <br />Example</span>: <br/><i>  width = 650 <br /> height = 400</i>'
                },
                {
                    type   : 'textbox',
                    name   : 'video_opts',
                    multiline: true,
                    label  : '',
                    tooltip : 'Please enter one per line following the example',
                    value  : options
                },
                {
                    type   : 'container',
                    name   : 'preview',
                    label  : 'Preview',
                    html   : '<div class="jwp-preview"> <div id="jwp-preview-tv"></div> </div>'
                },
            ]

        }

        function parseOptions(opts_string) {
            var opts = {};
            var lines = opts_string.split("\n");
            if (lines.length) {
                for (var i = 0; i < lines.length; i++) {
                    var line_parts = lines[i].split('=');
                    if (line_parts.length === 2) {
                        var key = $.trim(line_parts[0]);
                        var val = $.trim(line_parts[1]);
                        opts[key] = val;
                    }
                }
            }
            return opts;
        }

        function insertPlaceholder(data) {
            var $item = editor.selection && $(editor.selection.getNode());
            if (!$item.length) {
                $item.insertContent(getHTML(data));
            } else {
                $item.replaceWith(getHTML(data));
            }
        }

        function getHTML(opts) {
            delete opts['video_opts']
            var img_src = '../++plone++collective.jwplayer/dummy.gif';
            var width = opts.meta.width || 320;
            var height = opts.meta.height || 240;
            var $ele = $('<div />');
            var $embed = $('<img />');
            for (var opt in opts) {
                if (opt === 'meta') {
                    for (var meta_opt in opts['meta']) {
                        $embed.attr('data-meta_'+meta_opt, opts['meta'][meta_opt]);
                        opts['meta_'+meta_opt] = opts['meta'][meta_opt]
                    }
                } else {
                    $embed.attr('data-'+opt, opts[opt]);
                }
            }
            delete opts['meta']
            $embed.attr({'data-jwplayer': "'" + JSON.stringify(opts) + "'"})
            $embed.attr({'src': img_src, width: width, height: height});
            $embed.css({
                'background': "url('++plone++collective.jwplayer/jw.jpg')",
                'background-position': 'center center',
                'background-repeat': 'no-repeat',
                'background-color': '#efefef'
            })
            $embed.addClass('jwp-tinymce-player jwp-tinymce-plone jwp-tinymce-item');
            $ele.append($embed);
            return $ele.html();
        }

        /* Add a button that opens a window */
        editor.addButton('jwplayer', {
            //text: 'Insert Player',
            icon: 'media',
            text: 'Insert Video',
            tooltip: 'Insert Video',
            enabled: true,
            cmd: 'jwplayer',
            onClick: function () {
                /* Open window */
                openDialog();
            }
        });

        /* Adds a menu item, which can then be included in any menu via the menu/menubar configuration */
        editor.addMenuItem('jwplayer', {
            text: 'Insert Player',
            onClick: function () {
                /* Open window */
                openDialog();
            }
        });

        /* Return the metadata for the help plugin */
        return {
            getMetadata: function () {
                return {
                    name: 'JWPlayer Plugin',
                    url: 'https://psych.uni-goettingen.de/'
                };
            }
        };


    });
})(jQuery);

