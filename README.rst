.. This README is meant for consumption by humans and pypi. Pypi can render rst files so please do not use Sphinx features.
   If you want to learn more about writing documentation, please check out: http://docs.plone.org/about/documentation_styleguide.html
   This text does not appear on pypi or github. It is a comment.

.. image:: https://travis-ci.org/collective/collective.jwplayer.svg?branch=master
    :target: https://travis-ci.org/collective/collective.jwplayer

.. image:: https://coveralls.io/repos/github/collective/collective.jwplayer/badge.svg?branch=master
    :target: https://coveralls.io/github/collective/collective.jwplayer?branch=master
    :alt: Coveralls

.. image:: https://img.shields.io/pypi/v/collective.jwplayer.svg
    :target: https://pypi.python.org/pypi/collective.jwplayer/
    :alt: Latest Version

.. image:: https://img.shields.io/pypi/status/collective.jwplayer.svg
    :target: https://pypi.python.org/pypi/collective.jwplayer
    :alt: Egg Status

.. image:: https://img.shields.io/pypi/pyversions/collective.jwplayer.svg?style=plastic   :alt: Supported - Python Versions

.. image:: https://img.shields.io/pypi/l/collective.jwplayer.svg
    :target: https://pypi.python.org/pypi/collective.jwplayer/
    :alt: License


===================
collective.jwplayer
===================

Tell me what your product does

Features
--------

- Can be bullet points


Examples
--------

This add-on can be seen in action at the following sites:
- Is there a page on the internet where everybody can see the features?


Documentation
-------------

Full documentation for end users can be found in the "docs" folder, and is also available online at http://docs.plone.org/foo/bar


Translations
------------

This product has been translated into

- Klingon (thanks, K'Plai)


Installation
------------

Install collective.jwplayer by adding it to your buildout::

    [buildout]

    ...

    eggs =
        collective.jwplayer


and then running ``bin/buildout``


Contribute
----------

- Issue Tracker: https://github.com/collective/collective.jwplayer/issues
- Source Code: https://github.com/collective/collective.jwplayer
- Documentation: https://docs.plone.org/foo/bar


Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@example.com


License
-------

The project is licensed under the GPLv2.
